import React from "react";
import "./App.css";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

const CustomInput = ({ field, form, ...props }) => {
  return (
    <div className='form-group'>
      <label>{field.name}</label>
      <input {...field} {...props} type='text' className='form-control' />
    </div>
  );
};

const CustomError = (props) => {
  return <div className='text-danger'>{props.children}</div>;
};

function App() {
  const userSchema = Yup.object().shape({
    name: Yup.string()
      .min(3, "trop court")
      .max(7, "trop long")
      .required("required"),
    email: Yup.string().email("mauvais email").required("required"),
    password: Yup.string().min(5, "trop court"),
  });

  const submit = (values, actions) => {
    console.log(values);
    setTimeout(() => {
      actions.isSubmitting = false;
      actions.resetForm();
    }, 1000);
  };

  return (
    <div className='container p-5 bg-light d-flex flex-column justify-content-center left-align-items-center'>
      <Formik
        onSubmit={submit}
        initialValues={{ name: "", email: "", password: "" }}
        validationSchema={userSchema}>
        {({ handleSubmit, isSubmitting }) => (
          <form
            onSubmit={handleSubmit}
            className='bg-white border p-5 d-flex flex-column'>
            <Field name='name' component={CustomInput} />
            <ErrorMessage name='name' component={CustomError} />
            <Field name='email' type='email' component={CustomInput} />
            <ErrorMessage name='email' component={CustomError} />
            <Field name='password' component={CustomInput} />
            <ErrorMessage name='password' component={CustomError} />
            <button
              type='submit'
              className='btn btn-primary'
              disabled={isSubmitting}>
              Submit
            </button>
          </form>
        )}
      </Formik>
    </div>
  );
}

export default App;
